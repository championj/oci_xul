
function showMore() {
  document.getElementById("question").hidden = false;
  document.getElementById("text").hidden = false;
  document.getElementById("identifierOui").hidden = false;
  document.getElementById("identifierNon").hidden = false;
}

function showLess() {
  document.getElementById("text").hidden = true;
  document.getElementById("question").hidden = true;
  document.getElementById("identifierOui").hidden = true;
  document.getElementById("identifierNon").hidden = true;
}

function openWindow() {
  window.open("chrome://browser/content/places/places.xul", "bmarks", "chrome,width=600,height=300");    
}
